module XianqQi(xq_fen, xq_ubb) where

import Data.List.Split (chunksOf, splitOn)
import Data.Function (on)
import Data.List (sortBy, (\\), group, intercalate)
import Data.Char (isDigit)


-- utils ---------------------------------------------------------

ubbPieces :: String
ubbPieces = "RHEAKAEHRCCPPPPPrheakaehrccppppp"

toInt :: String -> Int
toInt x = read x :: Int

-- trie liste de paires selon premier element
sortOnFst :: (Ord a) => [(a, b)] -> [(a, b)] 
sortOnFst = sortBy (compare `on` fst)

-- coordonnees ubb -> index dans fen de longueur fixe
coordToIndex :: [Int] -> Int
coordToIndex [col, row] = col + 9 * row

-- index dans fen de longueur fixe -> coordonnees ubb
indexToCoord :: Int -> String
indexToCoord n = concatMap show [n `mod` 9, n `div` 9]

-- transforme les chiffres d'une chaine en autant de '='
fenrow :: String -> String
fenrow row = concatMap desAgregE $ chunksOf 1 row
  where desAgregE x = if isDigit (x !! 0)
                      then replicate (toInt x) '='
                      else x
 
-- transforme les suites de '=' en le nombre de '=' de la suite
stdrow :: String -> String
stdrow row = concatMap agregE $ group row
  where agregE x@('=':_) = show $ length x
        agregE x = x


-- exported ------------------------------------------------------

xq_fen :: String -> String
xq_fen ubb = intercalate "/" $ map stdrow rows
  where
    casesInt = map coordToIndex (chunksOf 2 $ map toInt $ chunksOf 1 ubb)
    occupC = filter (\x -> snd x < 90) $ zip ubbPieces casesInt
    freeC = zip (repeat '=') $ [0..89] \\ (map snd occupC)
    allC = sortBy (compare `on` snd) (occupC ++ freeC)
    rows = chunksOf 9 $ fst $ unzip allC

xq_ubb :: String -> String
xq_ubb fen = concatMap snd $ sortOnFst rankCoord
  where
    fen90 = concatMap fenrow $ splitOn "/" fen
    indexPieces = filter (\x -> fst x /= '=') $ zip fen90 [0..]
    coordPieces = map (\(x, y) -> (x, indexToCoord y)) indexPieces
    missing = zip (ubbPieces \\ map fst coordPieces) (repeat "99")
    refSort = sortOnFst $ zip ubbPieces [0..]
    posSort = sortOnFst $ coordPieces ++ missing
    rankCoord = zipWith (\(_, rank) (_, coord) -> (rank, coord)) refSort posSort
